/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseTrafficSinkAction.h"

#include <memory>

#include "Storyboard/GenericAction/TrafficSinkAction.h"

yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSinkAction> trafficSinkAction)
{
  return std::make_shared<TrafficSinkAction>(trafficSinkAction);
}
