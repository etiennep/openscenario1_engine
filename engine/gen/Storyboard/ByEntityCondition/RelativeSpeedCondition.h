/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/ByEntityCondition/RelativeSpeedCondition_impl.h"
#include "Utils/EntityBroker.h"

class RelativeSpeedCondition : public yase::ActionNode
{
public:
  RelativeSpeedCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeSpeedCondition> relativeSpeedCondition)
      : yase::ActionNode{"RelativeSpeedCondition"},
        relativeSpeedCondition_{relativeSpeedCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(entityBroker_);
    assert(impl_);

    const auto is_satisfied = impl_->IsSatisfied();

    if (is_satisfied)
    {
      entityBroker_->add_triggering(triggeringEntity_);
      return yase::NodeStatus::kSuccess;
    }

    return yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    triggeringEntity_ = blackboard.get<std::string>("TriggeringEntity");
    entityBroker_ = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::RelativeSpeedCondition>(
        OpenScenarioEngine::v1_1::RelativeSpeedCondition::Values{
            triggeringEntity_,
            OPENSCENARIO::ConvertScenarioEntity(relativeSpeedCondition_->GetEntityRef()),
            OPENSCENARIO::ConvertScenarioRule(relativeSpeedCondition_->GetRule(), relativeSpeedCondition_->GetValue())},
        OpenScenarioEngine::v1_1::RelativeSpeedCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::RelativeSpeedCondition> impl_{nullptr};
  std::string triggeringEntity_;
  EntityBroker::Ptr entityBroker_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeSpeedCondition> relativeSpeedCondition_;
};