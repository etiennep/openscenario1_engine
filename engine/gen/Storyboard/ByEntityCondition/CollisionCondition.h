/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/ByEntityCondition/CollisionCondition_impl.h"
#include "Utils/EntityBroker.h"

class CollisionCondition : public yase::ActionNode
{
public:
  CollisionCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICollisionCondition> collisionCondition)
      : yase::ActionNode{"CollisionCondition"},
        collisionCondition_{collisionCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(entityBroker_);
    assert(impl_);

    const auto is_satisfied = impl_->IsSatisfied();

    if (is_satisfied)
    {
      entityBroker_->add_triggering(triggeringEntity_);
      return yase::NodeStatus::kSuccess;
    }

    return yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    triggeringEntity_ = blackboard.get<std::string>("TriggeringEntity");
    entityBroker_ = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::CollisionCondition>(
        OpenScenarioEngine::v1_1::CollisionCondition::Values{
            triggeringEntity_,
            OPENSCENARIO::ConvertScenarioEntity(collisionCondition_->GetEntityRef()),
            OPENSCENARIO::ConvertScenarioByObjectType(collisionCondition_->GetByType())},
        OpenScenarioEngine::v1_1::CollisionCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::CollisionCondition> impl_{nullptr};
  std::string triggeringEntity_;
  EntityBroker::Ptr entityBroker_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICollisionCondition> collisionCondition_;
};