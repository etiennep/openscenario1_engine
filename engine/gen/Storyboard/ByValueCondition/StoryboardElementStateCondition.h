/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/ByValueCondition/StoryboardElementStateCondition_impl.h"

class StoryboardElementStateCondition : public yase::ActionNode
{
public:
  StoryboardElementStateCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IStoryboardElementStateCondition> storyboardElementStateCondition)
      : yase::ActionNode{"StoryboardElementStateCondition"},
        storyboardElementStateCondition_{storyboardElementStateCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    return impl_->IsSatisfied() ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::StoryboardElementStateCondition>(
        OpenScenarioEngine::v1_1::StoryboardElementStateCondition::Values{
            OPENSCENARIO::ConvertScenarioStoryboardElement(storyboardElementStateCondition_->GetStoryboardElementRef())},
        OpenScenarioEngine::v1_1::StoryboardElementStateCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::StoryboardElementStateCondition> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IStoryboardElementStateCondition> storyboardElementStateCondition_;
};