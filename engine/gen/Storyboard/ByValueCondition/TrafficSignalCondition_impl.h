/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <string>

namespace OpenScenarioEngine::v1_1
{
class TrafficSignalCondition
{
public:
  struct Values
  {
    std::string name;
    std::string state;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  TrafficSignalCondition(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{interfaces} {};

  bool IsSatisfied() const;

private:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1