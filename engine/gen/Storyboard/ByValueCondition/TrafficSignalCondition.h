/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/ByValueCondition/TrafficSignalCondition_impl.h"

class TrafficSignalCondition : public yase::ActionNode
{
public:
  TrafficSignalCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalCondition> trafficSignalCondition)
      : yase::ActionNode{"TrafficSignalCondition"},
        trafficSignalCondition_{trafficSignalCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    return impl_->IsSatisfied() ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::TrafficSignalCondition>(
        OpenScenarioEngine::v1_1::TrafficSignalCondition::Values{
            trafficSignalCondition_->GetName(),
            trafficSignalCondition_->GetState()},
        OpenScenarioEngine::v1_1::TrafficSignalCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::TrafficSignalCondition> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalCondition> trafficSignalCondition_;
};