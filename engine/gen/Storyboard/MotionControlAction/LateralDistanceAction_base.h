/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"
#include "Conversion/OscToMantle/ConvertScenarioDynamicConstraints.h"
#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class LateralDistanceActionBase
{
public:
  struct Values
  {
    Entities entities;
    bool continuous;
    double distance;
    bool freespace;
    OPENSCENARIO::DynamicConstraints dynamicConstraints;
    OPENSCENARIO::Entity entityRef;
    OPENSCENARIO::CoordinateSystem coordinateSystem;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  LateralDistanceActionBase(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{interfaces} {};

  virtual void SetControlStrategy() = 0;
  virtual bool HasControlStrategyGoalBeenReached(const std::string& actor) = 0;
  virtual mantle_api::MovementDomain GetMovementDomain() const = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_1