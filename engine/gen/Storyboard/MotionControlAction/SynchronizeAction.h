/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/MotionControlAction/MotionControlAction.h"
#include "Storyboard/MotionControlAction/SynchronizeAction_impl.h"
#include "Utils/EntityBroker.h"

class SynchronizeAction : public yase::ActionNode
{
public:
  SynchronizeAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISynchronizeAction> synchronizeAction)
      : yase::ActionNode{"SynchronizeAction"},
        synchronizeAction_{synchronizeAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::MotionControlAction<OpenScenarioEngine::v1_1::SynchronizeAction>>(
        OpenScenarioEngine::v1_1::SynchronizeAction::Values{
            entityBroker->GetEntites(),
            synchronizeAction_->GetTargetTolerance(),
            synchronizeAction_->GetTargetToleranceMaster(),
            OPENSCENARIO::ConvertScenarioPosition(environment, synchronizeAction_->GetTargetPositionMaster()),
            OPENSCENARIO::ConvertScenarioPosition(environment, synchronizeAction_->GetTargetPosition()),
            OPENSCENARIO::ConvertScenarioFinalSpeed(environment, synchronizeAction_->GetFinalSpeed()),
            OPENSCENARIO::ConvertScenarioEntity(synchronizeAction_->GetMasterEntityRef())},
        OpenScenarioEngine::v1_1::SynchronizeAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::MotionControlAction<OpenScenarioEngine::v1_1::SynchronizeAction>> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ISynchronizeAction> synchronizeAction_;
};