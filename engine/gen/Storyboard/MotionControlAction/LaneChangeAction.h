/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/MotionControlAction/LaneChangeAction_impl.h"
#include "Storyboard/MotionControlAction/MotionControlAction.h"
#include "Utils/EntityBroker.h"

class LaneChangeAction : public yase::ActionNode
{
public:
  LaneChangeAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILaneChangeAction> laneChangeAction)
      : yase::ActionNode{"LaneChangeAction"},
        laneChangeAction_{laneChangeAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::MotionControlAction<OpenScenarioEngine::v1_1::LaneChangeAction>>(
        OpenScenarioEngine::v1_1::LaneChangeAction::Values{
            entityBroker->GetEntites(),
            laneChangeAction_->GetTargetLaneOffset(),
            OPENSCENARIO::ConvertScenarioTransitionDynamics(laneChangeAction_->GetLaneChangeActionDynamics()),
            [=]() { return OPENSCENARIO::ConvertScenarioLaneChangeTarget(environment, laneChangeAction_->GetLaneChangeTarget()); }},
        OpenScenarioEngine::v1_1::LaneChangeAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::MotionControlAction<OpenScenarioEngine::v1_1::LaneChangeAction>> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILaneChangeAction> laneChangeAction_;
};