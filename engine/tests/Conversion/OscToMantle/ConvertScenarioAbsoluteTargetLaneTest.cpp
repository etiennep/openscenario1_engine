/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_1/impl/ApiClassImplV1_1.h>

#include "Conversion/OscToMantle/ConvertScenarioAbsoluteTargetLane.h"

TEST(ConvertScenarioAbsoluteTargetLaneTest, GivenNumberOfTheTargetLaneAsString_ThenConvertIntoLong)
{
  auto absolute_target_lane_ = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::AbsoluteTargetLaneImpl>();
  absolute_target_lane_->SetValue("10");

  const auto id = OPENSCENARIO::ConvertScenarioAbsoluteTargetLane(absolute_target_lane_);

  ASSERT_EQ(mantle_api::UniqueId(10), id);
}