/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/RelativeSpeedCondition_impl.h"

using namespace mantle_api;
using namespace units::literals;
using testing::Return;
using testing::ReturnRef;

TEST(RelativeSpeedConditionTest, GivenSpeedDiffLessthanTolerance_ReturnTrue)
{
  auto rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 10.0);
  auto mockEnvironment = std::make_shared<MockEnvironment>();

  OpenScenarioEngine::v1_1::RelativeSpeedCondition relativeSpeedCondition({"Vehicle1",
                                                                           "Ego",
                                                                           rule},
                                                                          {mockEnvironment});
}