/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByValueCondition/SimulationTimeCondition_impl.h"

using namespace mantle_api;

TEST(SimulationTimeCondition_UnitTest, HasSimulationTimeConditionMet)
{
  auto rule = OPENSCENARIO::Rule(NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, 1.0);
  auto mockEnvironment = std::make_shared<MockEnvironment>();

  EXPECT_CALL(*mockEnvironment, GetSimulationTime()).Times(1);

  OpenScenarioEngine::v1_1::SimulationTimeCondition simulationTimeCondition({rule},
                                                                            {mockEnvironment});

  EXPECT_NO_THROW(simulationTimeCondition.IsSatisfied());
}