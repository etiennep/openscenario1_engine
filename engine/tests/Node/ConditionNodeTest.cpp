/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <agnostic_behavior_tree/decorator/data_declaration_node.h>
#include <gtest/gtest.h>

#include <memory>

#include "Node/ConditionNode.h"
#include "Node/Testing/FakeActionNode.h"

using namespace mantle_api;
using namespace units::literals;

using testing::FakeActionNode;
using testing::Return;

using namespace NET_ASAM_OPENSCENARIO::v1_1;

class TreeWithEnvironment
{
public:
  TreeWithEnvironment()
      : environment_{std::make_shared<mantle_api::MockEnvironment>()}
  {
    auto declaration = std::make_unique<
        yase::TypeDeclarer<
            std::shared_ptr<
                mantle_api::IEnvironment>>>("Environment", environment_);
    root_ = std::make_shared<yase::DataDeclarationNode>("RootWithEnvironment", std::move(declaration));
  }

  yase::DecoratorNode& get_root()
  {
    return *root_;
  }

  mantle_api::MockEnvironment& get_environment()
  {
    return *environment_;
  }

private:
  std::shared_ptr<mantle_api::MockEnvironment> environment_;
  yase::DataDeclarationNode::Ptr root_;
};

TEST(ConditionNode, GivenNoDelay_WhenChildConditionSucceeds_SucceedsImmediatly)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  ON_CALL(environment, GetSimulationTime()).WillByDefault(Return(0_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  ON_CALL(*mockActionNodePtr, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));

  constexpr double NO_DELAY{0.0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", NO_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
}

TEST(ConditionNode, GivenDelay_WhenChildConditionIsFullfilled_DoesNotSucceedImmediatly)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  ON_CALL(environment, GetSimulationTime()).WillByDefault(Return(0_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  ON_CALL(*mockActionNodePtr, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));

  constexpr double TEN_SEC_DELAY{10.0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", TEN_SEC_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
}

TEST(ConditionNode, GivenDelay_WhenChildConditionIsFullfilledImmediatly_SucceedsAfterDelay)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillRepeatedly(Return(10_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  ON_CALL(*mockActionNodePtr, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));

  constexpr double TEN_SEC_DELAY{10.0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", TEN_SEC_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);
  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // t = 0_S
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);  // t = 10_s
}

TEST(ConditionNode, GivenDelay_WhenChildConditionIsFullfilledAfterSomeTime_SucceedsAfterAdditionalDelay)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(10_s))
      .WillRepeatedly(Return(20_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  EXPECT_CALL(*mockActionNodePtr, tick())
      .WillOnce(Return(yase::NodeStatus::kRunning))         // 0_s
      .WillRepeatedly(Return(yase::NodeStatus::kSuccess));  // 10_s

  constexpr double TEN_SEC_DELAY{10.0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", TEN_SEC_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // t = 0_S
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // t = 10_s
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);  // t = 20_s
}

TEST(ConditionNode, GivenNoDelay_WhenChildConditionIsNotFullfilled_NeverSucceeds)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(100_s))
      .WillOnce(Return(200_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  ON_CALL(*mockActionNodePtr, tick()).WillByDefault(Return(yase::NodeStatus::kRunning));

  constexpr double NO_DELAY{0.0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", NO_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 0_s
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 100_s
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 200_s
}

TEST(ConditionNode, GivenNoDelay_WhenChildConditionIsNotFullfilledAnymore_FallsBackToRunningImmediatly)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillRepeatedly(Return(10_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  EXPECT_CALL(*mockActionNodePtr, tick())
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kRunning));

  constexpr double NO_DELAY{0.0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", NO_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);  // 0_s
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 10_s
}

TEST(ConditionNode, GivenDelay_WhenChildConditionIsNotFullfilledAnymore_FallsBackToRunningAfterDelay)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(10_s))
      .WillRepeatedly(Return(20_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  EXPECT_CALL(*mockActionNodePtr, tick())
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillRepeatedly(Return(yase::NodeStatus::kRunning));

  constexpr double TEN_SEC_DELAY{10.0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", TEN_SEC_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 0_s
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);  // 10_s
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 20_s
}

TEST(ConditionNode, GivenDelayNotDivisableByStepTime_WhenChildConditionIsFullfilled_SucceedsAfterDelay)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(1_s))
      .WillRepeatedly(Return(2_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  ON_CALL(*mockActionNodePtr, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));

  constexpr double UNDIVISEABLE_DELAY{1.23};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", UNDIVISEABLE_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 0_s -> success of child
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);  // 1_s -> not yet...
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);  // 2_s
}

TEST(ConditionNode, GivenRisingEdge_WhenChildConditionIsChangingToFullfilled_Succeeds)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(1_s))
      .WillOnce(Return(2_s))
      .WillOnce(Return(3_s))
      .WillOnce(Return(4_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  EXPECT_CALL(*mockActionNodePtr, tick())
      .WillOnce(Return(yase::NodeStatus::kRunning))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kRunning));

  constexpr double NO_DELAY{0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", NO_DELAY, OPENSCENARIO::ConditionEdge::kRising, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
}

TEST(ConditionNode, GivenFallingEdge_WhenChildConditionIsNotFullfilledAnymore_Succeeds)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(1_s))
      .WillOnce(Return(2_s))
      .WillOnce(Return(3_s))
      .WillOnce(Return(4_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  EXPECT_CALL(*mockActionNodePtr, tick())
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kRunning))
      .WillOnce(Return(yase::NodeStatus::kRunning))
      .WillOnce(Return(yase::NodeStatus::kRunning))
      .WillOnce(Return(yase::NodeStatus::kSuccess));

  constexpr double NO_DELAY{0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", NO_DELAY, OPENSCENARIO::ConditionEdge::kFalling, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
}

TEST(ConditionNode, GivenRisingOrFallingEdge_WhenChildConditionIsChangingState_Succeeds)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(1_s))
      .WillOnce(Return(2_s))
      .WillOnce(Return(3_s))
      .WillOnce(Return(4_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  EXPECT_CALL(*mockActionNodePtr, tick())
      .WillOnce(Return(yase::NodeStatus::kRunning))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kRunning));

  constexpr double NO_DELAY{0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", NO_DELAY, OPENSCENARIO::ConditionEdge::kRisingOrFalling, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
}

TEST(ConditionNode, GivenNoEdge_WhenChildConditionIsFullfilled_Succeeds)
{
  TreeWithEnvironment tree;
  auto& root = tree.get_root();
  auto& environment = tree.get_environment();
  EXPECT_CALL(environment, GetSimulationTime())
      .WillOnce(Return(0_s))
      .WillOnce(Return(1_s))
      .WillOnce(Return(2_s))
      .WillOnce(Return(3_s))
      .WillOnce(Return(4_s));

  auto mockActionNodePtr = FakeActionNode::CREATE_PTR();
  EXPECT_CALL(*mockActionNodePtr, tick())
      .WillOnce(Return(yase::NodeStatus::kRunning))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kSuccess))
      .WillOnce(Return(yase::NodeStatus::kRunning))
      .WillOnce(Return(yase::NodeStatus::kSuccess));

  constexpr double NO_DELAY{0};
  auto condition_under_test = std::make_shared<yase::ConditionNode>(
      "condition under test", NO_DELAY, OPENSCENARIO::ConditionEdge::kNone, mockActionNodePtr);

  root.setChild(condition_under_test);
  root.distributeData();
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
}
