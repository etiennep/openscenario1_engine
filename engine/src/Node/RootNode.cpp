/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/RootNode.h"

#include "Conversion/OscToNode/ParseStoryboard.h"
#include "Conversion/OscToNode/ParseTrafficSignals.h"

namespace OPENSCENARIO
{
RootNode::RootNode(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioDefinition> scenarioDefinition,
                   std::shared_ptr<mantle_api::IEnvironment> environment,
                   std::shared_ptr<EngineAbortFlags> engine_abort_flags)
    : yase::ParallelNode{NAME_NODE_ROOT},
      environment_{environment},
      engine_abort_flags_{engine_abort_flags}
{
  if (auto storyboard = scenarioDefinition->GetStoryboard(); storyboard)
  {
    addChild(::parse(storyboard));
  }
  if (const auto& traffic_signals = scenarioDefinition->GetRoadNetwork()->GetTrafficSignals(); !traffic_signals.empty())
  {
    addChild(::parse(traffic_signals));
  }
  distributeData();
}

}  // namespace OPENSCENARIO