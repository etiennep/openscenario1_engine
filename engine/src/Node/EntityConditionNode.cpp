/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/EntityConditionNode.h"

#include "Conversion/OscToNode/ParseEntityCondition.h"

EntityConditionNode::EntityConditionNode(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityCondition> entityCondition,
    const OPENSCENARIO::Entity triggeringEntity)
    : ParallelNode{"EntityCondition"},
      triggeringEntity_{std::move(triggeringEntity)}
{
  addChild(parse(entityCondition));
}

void EntityConditionNode::lookupAndRegisterData(yase::Blackboard &blackboard)
{
  blackboard.set("TriggeringEntity", triggeringEntity_);
}
