/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/decorator_node.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <string>

#include "Utils/ConditionEdgeEvaluator.h"

namespace yase
{
/// Checks if underlying conditions are satisfied and adds control flow logic, such as delayed execution
///
/// \see https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/index.html
class ConditionNode : public yase::DecoratorNode
{
public:
  /// Creates a condition node
  ///
  /// @param name  Name of the condition
  /// @param delay Time elapsed after the edge condition is verified, until the condition report success. Unit: s
  /// @param condition_edge Determines which to which condition edge the condition shall be sensitive
  /// @param child Either a ByValueCondition or a ByEntityCondition
  explicit ConditionNode(std::string name,
                         double delay,
                         OPENSCENARIO::ConditionEdge condition_edge,
                         yase::BehaviorNode::Ptr child);

  ~ConditionNode() override = default;

  void onInit() final{};

  void lookupAndRegisterData(yase::Blackboard& blackboard) final;

private:
  NodeStatus tick() final;
  void UpdateBuffer(mantle_api::Time previous_result_time);

  std::shared_ptr<mantle_api::IEnvironment> environment_;  //!< access to the environment (e.g. current simulation time)
  std::optional<mantle_api::Time> start_time_;             //!< time when child condition has succeeded (nullopt if not yet succeeded)
  mantle_api::Time delay_;                                 //!< delay in s before child success will be reported
  std::map<mantle_api::Time, bool> result_buffer_;         //!< storage for delayed events
  OPENSCENARIO::ConditionEdgeEvaluator edge_evaluator_;    //!< used to evaluate the edge of the child conditions
};

}  // namespace yase