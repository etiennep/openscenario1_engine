/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <iostream>

namespace OPENSCENARIO
{
enum class RelativeDistanceType
{
  kUnknown,
  kCartesian_distance,
  kEuclidian_distance,
  kLateral,
  kLongitudinal
};

RelativeDistanceType ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_1::RelativeDistanceType relativeDistanceType);

}  // namespace OPENSCENARIO