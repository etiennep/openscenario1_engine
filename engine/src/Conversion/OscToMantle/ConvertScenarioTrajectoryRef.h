
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/catalog/CatalogHelperV1_1.h>

#include <memory>
#include <optional>
#include <string>

namespace OPENSCENARIO
{
using TrajectoryRef = std::optional<mantle_api::Trajectory>;

TrajectoryRef ConvertScenarioTrajectoryRef(std::shared_ptr<mantle_api::IEnvironment> environment,
                                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryRef> trajectoryRef,
                                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference);

}  // namespace OPENSCENARIO