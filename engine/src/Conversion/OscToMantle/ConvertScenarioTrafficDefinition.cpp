
/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "engine/src/Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"
#include "engine/src/Conversion/OscToMantle/ConvertScenarioController.h"

namespace OPENSCENARIO
{

namespace {

mantle_api::VehicleClass ConvertVehicleCategoryDistributionEntryCategory(NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory vehicle_category)
{
  mantle_api::VehicleClass vehicle_class;

  const auto literal{vehicle_category.GetLiteral()};
  const auto category{vehicle_category.GetFromLiteral(literal)};

  switch(category)
  {
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::UNKNOWN:
    {
      vehicle_class = mantle_api::VehicleClass::kInvalid;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::BICYCLE:
    {
      vehicle_class = mantle_api::VehicleClass::kBicycle;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::CAR:
    {
      vehicle_class = mantle_api::VehicleClass::kMedium_car;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::MOTORBIKE:
    {
      vehicle_class = mantle_api::VehicleClass::kMotorbike;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::SEMITRAILER:
    {
      vehicle_class = mantle_api::VehicleClass::kSemitrailer;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::TRAILER:
    {
      vehicle_class = mantle_api::VehicleClass::kTrailer;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::TRAIN:
    {
      vehicle_class = mantle_api::VehicleClass::kTrain;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::TRAM:
    {
      vehicle_class = mantle_api::VehicleClass::kTram;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::TRUCK:
    {
      vehicle_class = mantle_api::VehicleClass::kHeavy_truck;
      break;
    }
    case NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum::VAN:
    {
      vehicle_class = mantle_api::VehicleClass::kDelivery_van;
      break;
    }
    default:
      vehicle_class = mantle_api::VehicleClass::kInvalid;
      break;
  }
}

mantle_api::VehicleCategoryDistribution ConvertVehicleCategoryDistributionEntry(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVehicleCategoryDistributionEntry> entry)
{
  mantle_api::VehicleCategoryDistribution vehicle_category_distribution{};

  if(entry->IsSetCategory())
  {
    vehicle_category_distribution.category = ConvertVehicleCategoryDistributionEntryCategory(entry->GetCategory());
  }

  if(entry->IsSetWeight())
  {
    vehicle_category_distribution.weight = entry->GetWeight();
  }

  return vehicle_category_distribution;
}

mantle_api::ControllerDistribution ConvertControllerDistributionEntry(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IControllerDistributionEntry> entry)
{
  mantle_api::ControllerDistribution controller_distribution{};
  controller_distribution.weight = entry->GetWeight();
  controller_distribution.controller = ConvertScenarioController(entry->GetController(), entry->GetCatalogReference());
  return controller_distribution;
}

} // namespace

inline TrafficDefinition ConvertScenarioTrafficDefinition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficDefinition> trafficDefinition)
{
  std::vector<mantle_api::VehicleCategoryDistribution> vehicleCategoryDistributionEntries{};

  for(const auto& vehicle_category_distribution_entry : trafficDefinition->GetVehicleCategoryDistribution()->GetVehicleCategoryDistributionEntries())
  {
    vehicleCategoryDistributionEntries.push_back(ConvertVehicleCategoryDistributionEntry(vehicle_category_distribution_entry));
  }

  std::vector<mantle_api::ControllerDistribution> controllerDistributionEntries{};

  for(const auto& controller_distribution_entry : trafficDefinition->GetControllerDistribution()->GetControllerDistributionEntries())
  {
    controllerDistributionEntries.push_back(ConvertControllerDistributionEntry(controller_distribution_entry));
  }

  TrafficDefinition traffic_definition;
  traffic_definition.name = trafficDefinition->GetName();
  traffic_definition.vehicleCategoryDistributionEntries = vehicleCategoryDistributionEntries;
  traffic_definition.controllerDistributionEntries = controllerDistributionEntries;

  return traffic_definition;
}

}  // namespace OPENSCENARIO