#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>

#include <memory>
#include <string>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"

namespace OPENSCENARIO
{
struct ReachPositionCondition
{
  double tolerance;
  mantle_api::Pose pose;
};

inline ReachPositionCondition ConvertScenarioReachPositionCondition(
    std::shared_ptr<mantle_api::IEnvironment> environment,
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IReachPositionCondition> reachPositionCondition)
{
  return {
      reachPositionCondition->GetTolerance(),
      ConvertScenarioPosition(environment, reachPositionCondition->GetPosition())

  };
}

}  // namespace OPENSCENARIO