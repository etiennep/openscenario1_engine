
/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>
#include <openScenarioLib/generated/v1_1/catalog/CatalogHelperV1_1.h>

#include <memory>
#include <string>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"

namespace OPENSCENARIO
{
struct Route
{
  bool closed{false};
  std::vector<mantle_api::RouteWaypoint> waypoints;
};

Route ConvertScenarioRoute(std::shared_ptr<mantle_api::IEnvironment> environment,
                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRoute> route,
                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference);

}  // namespace OPENSCENARIO