/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseAction.h"

#include <memory>

#include "Conversion/OscToNode/ParseGlobalAction.h"
#include "Conversion/OscToNode/ParsePrivateAction.h"
#include "Conversion/OscToNode/ParseUserDefinedAction.h"

yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAction> action)
{
  if (auto element = action->GetGlobalAction(); element)
  {
    return parse(element);
  }
  if (auto element = action->GetUserDefinedAction(); element)
  {
    return parse(element);
  }
  if (auto element = action->GetPrivateAction(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_1::IAction");
}
