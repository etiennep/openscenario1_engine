/********************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Traffic/control_strategy.h>

#include <algorithm>
#include <memory>
#include <type_traits>
#include <vector>

namespace OpenScenarioEngine::v1_1
{
template <typename Action>
class MotionControlAction : public Action
{
public:
  MotionControlAction(typename Action::Values values, typename Action::Interfaces interfaces)
      : Action{values, interfaces},
        pending_actors{values.entities},
        environment{interfaces.environment},
        action_{*(static_cast<Action *>(this))}
  {
  }

  bool Step()
  {
    if (!control_strategy_set_)
    {
      action_.SetControlStrategy();
      control_strategy_set_ = true;
    }

    std::vector<std::string> newly_finished_actors;
    for (const auto &actor : pending_actors)
    {
      if (action_.HasControlStrategyGoalBeenReached(actor))
      {
        if (const auto &entity = environment->GetEntityRepository().Get(actor); entity)
        {
          const auto movement_domain = action_.GetMovementDomain();
          if (movement_domain == mantle_api::MovementDomain::kLongitudinal || movement_domain == mantle_api::MovementDomain::kBoth)
          {
            environment->UpdateControlStrategies(
                entity.value().get().GetUniqueId(),
                {std::make_shared<mantle_api::KeepVelocityControlStrategy>()});
          }
          if (movement_domain == mantle_api::MovementDomain::kLateral || movement_domain == mantle_api::MovementDomain::kBoth)
          {
            environment->UpdateControlStrategies(
                entity.value().get().GetUniqueId(),
                {std::make_shared<mantle_api::KeepLaneOffsetControlStrategy>()});
          }
        }

        newly_finished_actors.push_back(actor);
      }
    }

    for (const auto &actor : newly_finished_actors)
    {
      pending_actors.erase(std::find(pending_actors.begin(), pending_actors.end(), actor));
    }

    return pending_actors.empty();
  }

private:
  bool control_strategy_set_{false};

  Action &action_;
  std::vector<std::string> pending_actors;
  std::shared_ptr<mantle_api::IEnvironment> environment;
};

}  // namespace OpenScenarioEngine::v1_1
