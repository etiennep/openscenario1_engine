/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/FollowTrajectoryAction_impl.h"

#include <MantleAPI/Traffic/entity_helper.h>

#include <iostream>

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_1
{
void FollowTrajectoryAction::SetControlStrategy()
{
  if (!values.trajectoryRef)
  {
    throw std::runtime_error(
        "FollowTrajectoryAction does not contain TrajectoryRef. "
        "Note: Deprecated element Trajectory is not supported.");
  }

  for (const auto& entity : values.entities)
  {
    const auto& refEntity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, entity);
    auto control_strategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
    for (auto& polyLinePoint : std::get<mantle_api::PolyLine>(values.trajectoryRef->type))
    {
      polyLinePoint.pose.position = mantle.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(polyLinePoint.pose.position,
                                                                                                            polyLinePoint.pose.orientation,
                                                                                                            refEntity.GetProperties()->bounding_box.geometric_center);
    }

    control_strategy->trajectory = values.trajectoryRef.value();
    control_strategy->timeReference = values.timeReference;

    const auto entity_id = mantle.environment->GetEntityRepository().Get(entity)->get().GetUniqueId();
    mantle.environment->UpdateControlStrategies(entity_id, {control_strategy});
  }
}

bool FollowTrajectoryAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  if (auto entity = mantle.environment->GetEntityRepository().Get(actor))
  {
    return mantle.environment->HasControlStrategyGoalBeenReached(
        entity.value().get().GetUniqueId(),
        mantle_api::ControlStrategyType::kFollowTrajectory);
  }
  return false;
}

mantle_api::MovementDomain FollowTrajectoryAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kBoth;
}

}  // namespace OpenScenarioEngine::v1_1