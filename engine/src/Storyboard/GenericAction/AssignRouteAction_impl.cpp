/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/AssignRouteAction_impl.h"

#include <stdexcept>

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_1
{
bool AssignRouteAction::Step()
{
  if (values.route.closed)
  {
    std::cout << "AssignRouteAction: Closed routes not supported yet\n";
  }

  for (const auto& actor : values.entities)
  {
    const auto& entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, actor);
    mantle.environment->AssignRoute(entity.GetUniqueId(), {values.route.waypoints});
  }
  return true;
}

}  // namespace OpenScenarioEngine::v1_1
