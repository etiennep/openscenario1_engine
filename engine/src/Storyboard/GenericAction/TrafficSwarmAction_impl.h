/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Storyboard/GenericAction/TrafficSwarmAction_base.h"

namespace OpenScenarioEngine::v1_1
{
class TrafficSwarmAction : public TrafficSwarmActionBase
{
public:
  TrafficSwarmAction(Values values, Interfaces interfaces);

  bool Step() override;

private:
  void DespawnEntities();
  void SpawnEntities();
  void RemoveEntity(const std::vector<std::pair<mantle_api::UniqueId, mantle_api::UniqueId>>::iterator& iterator);
  mantle_api::VehicleClass GetVehicleClassification();
  mantle_api::IControllerConfig GetControllerConfiguration();
  
  const double vehicle_category_distribution_weights_sum;
  const double controller_distribution_weights_sum;

  std::vector<std::pair<mantle_api::UniqueId, mantle_api::UniqueId>> entity_and_controller_id_list_;
  size_t spawned_entities_count_{0};
};

}  // namespace OpenScenarioEngine::v1_1