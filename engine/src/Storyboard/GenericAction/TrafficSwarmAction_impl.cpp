/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/TrafficSwarmAction_impl.h"
#include "engine/src/Utils/EntityUtils.h"

#include "MantleAPI/Traffic/entity_properties.h"
#include "MantleAPI/Traffic/i_controller_config.h"

#include <iostream>
#include <cmath>
#include <random>

namespace OpenScenarioEngine::v1_1
{

namespace
{

// taken from :
// https://math.stackexchange.com/questions/76457/check-if-a-point-is-within-an-ellipse
// https://www.geeksforgeeks.org/check-if-a-point-is-inside-outside-or-on-the-ellipse/
// https://stackoverflow.com/questions/7946187/point-and-ellipse-rotated-position-test-algorithm
bool IsPointOutsideOfEllipsis(const mantle_api::Vec3<units::length::meter_t>& point,
                              const mantle_api::Vec3<units::length::meter_t>& ellipsis_center,
                              double ellipsis_semi_major_axis,
                              double ellipsis_semi_minor_axis,
                              units::angle::radian_t ellipsis_angle)
{
  const auto cos_angle{std::cos(ellipsis_angle())};
  const auto sin_angle{std::sin(ellipsis_angle())};

  const auto x_diff{point.x - ellipsis_center.x};
  const auto y_diff{point.y - ellipsis_center.y};

  const auto a{std::pow((cos_angle * x_diff()) + (sin_angle * y_diff()), 2)};
  const auto b{std::pow((sin_angle * x_diff()) - (cos_angle * y_diff()), 2)};

  const auto ellipse{(a / std::pow(ellipsis_semi_minor_axis, 2)) + (b / std::pow(ellipsis_semi_major_axis, 2))};

  return ellipse > 1.0;
}

double GenerateRandomPositiveDouble(double maximum)
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.0, maximum);
  return dis(gen);
}

mantle_api::Vec3<units::velocity::meters_per_second_t> GetVelocityVector(double speed, const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
  auto cos_elevation{std::cos(orientation.pitch())};
  return {units::velocity::meters_per_second_t(speed * std::cos(orientation.yaw()) * cos_elevation),
          units::velocity::meters_per_second_t(speed * std::sin(orientation.yaw()) * cos_elevation),
          units::velocity::meters_per_second_t(speed * -std::sin(orientation.pitch()))};
}

} // namespace

TrafficSwarmAction::TrafficSwarmAction(Values values, Interfaces interfaces)
: TrafficSwarmActionBase{std::move(values), interfaces},
vehicle_category_distribution_weights_sum{std::accumulate(values.trafficDefinition.vehicleCategoryDistributionEntries.begin(),
                                                          values.trafficDefinition.vehicleCategoryDistributionEntries.end(),
                                                          0.0,
                                                          [](auto a, auto b) { return a.weight + b.weight; })},
controller_distribution_weights_sum{std::accumulate(values.trafficDefinition.controllerDistributionEntries.begin(),
                                                    values.trafficDefinition.controllerDistributionEntries.end(),
                                                    0.0,
                                                    [](auto a, auto b) { return a.weight + b.weight; })}
{
  mantle.environment->InitTrafficSwarmController();
}

mantle_api::VehicleClass TrafficSwarmAction::GetVehicleClassification()
{
  const auto random{GenerateRandomPositiveDouble(vehicle_category_distribution_weights_sum)};

  for(const auto& vehicle_category_distribution_entry : values.trafficDefinition.vehicleCategoryDistributionEntries)
  {
    if(random < vehicle_category_distribution_entry.weight)
    {
      return vehicle_category_distribution_entry.category;
    }

    random -= vehicle_category_distribution_entry.weight;
  }
}

mantle_api::IControllerConfig TrafficSwarmAction::GetControllerConfiguration()
{
  const auto random{GenerateRandomPositiveDouble(controller_distribution_weights_sum)};

  for(const auto& controller_distribution_entry : values.trafficDefinition.vehicleCategoryDistributionEntries)
  {
    if(random < controller_distribution_entry.weight)
    {
      return controller_distribution_entry.controller;
    }

    random -= controller_distribution_entry.weight;
  }
}

void TrafficSwarmAction::RemoveEntity(const std::vector<std::pair<mantle_api::UniqueId, mantle_api::UniqueId>>::iterator& iterator)
{
  mantle.environment->RemoveControllerFromEntity(iterator->second);
  mantle.environment->GetEntityRepository().Delete(iterator->first);
  entity_and_controller_id_list_.erase(iterator);
}

bool TrafficSwarmAction::Step()
{
  DespawnEntities();
  SpawnEntities();

  return true;
}

void TrafficSwarmAction::DespawnEntities()
{
  const auto& entities{mantle.environment->GetEntityRepository().GetEntities()};

  for (size_t i{0}; i < entity_and_controller_id_list_.size(); ++i)
  {
    const auto& entity_and_controllers_ids{entity_and_controller_id_list_.at(i)};
    const auto entity_iterator{std::find_if(entities.cbegin(), entities.cend(), [entity_and_controllers_ids](auto& entity) { return entity_and_controllers_ids.first == entity.GetUniqueId(); })};

    if(entity_iterator == std::end(entities))
    {
      RemoveEntity(entity_and_controller_id_list_.begin() + i);
      continue;
    }

    const auto ellipsis_center_angle{mantle.environment->GetTrafficSwarmController().GetEllipsisCenterAndAngle()};

    const bool entity_is_outside_ellipsis{IsPointOutsideOfEllipsis(entity_iterator->GetPosition(),
                                                                   ellipsis_center_angle.first,
                                                                   values.semiMajorAxis,
                                                                   values.semiMinorAxis,
                                                                   ellipsis_center_angle.second)};

    if(entity_is_outside_ellipsis)
    {
      RemoveEntity(entity_and_controller_id_list_.begin() + i);
    }
  }
}

void TrafficSwarmAction::SpawnEntities()
{
  const auto central_object_position{OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, values.centralObject.entityRef).GetPosition()};
  const auto spawning_poses{mantle.environment->GetTrafficSwarmController().GetAvailableSpawningPosesAroundPoint(central_object_position)};

  for(const auto& spawning_pose : spawning_poses)
  {
    mantle_api::VehicleProperties properties;
    properties.classification = GetVehicleClassification();
    properties.is_controlled_externally = true;
    properties.is_host = false;
    properties.type = mantle_api::EntityType::kVehicle;

    auto& entity{mantle.environment->GetEntityRepository().Create("swarm_entity_" + std::to_string(spawned_entities_count_), properties)};
    entity.SetPosition(spawning_pose.position);
    entity.SetOrientation(spawning_pose.orientation);
    entity.SetVelocity(GetVelocityVector(values.velocity, spawning_pose.orientation));

    auto controller_configuration{std::make_unique<mantle_api::IControllerConfig>(GetControllerConfiguration())};
    const auto& controller{mantle.environment->GetControllerRepository().Create(std::move(controller_configuration))};

    mantle.environment->AddEntityToController(entity, controller.GetUniqueId());

    entity_and_controller_id_list_.push_back({entity.GetUniqueId(), controller.GetUniqueId()});

    spawned_entities_count_++;
  }
}

}  // namespace OpenScenarioEngine::v1_1