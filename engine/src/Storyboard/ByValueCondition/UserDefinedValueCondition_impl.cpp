/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByValueCondition/UserDefinedValueCondition_impl.h"

#include <iostream>

namespace OpenScenarioEngine::v1_1
{
bool UserDefinedValueCondition::IsSatisfied() const
{
  if (auto user_defined_value = mantle.environment->GetUserDefinedValue(values.name))
  {
    return values.rule.IsSatisfied(std::move(user_defined_value.value()));
  }
  return false;
}

}  // namespace OpenScenarioEngine::v1_1