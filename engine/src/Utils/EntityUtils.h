/********************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Traffic/i_entity_repository.h>

#include "Utils/EntityBroker.h"

namespace OPENSCENARIO
{
using namespace units::literals;

class EntityUtils
{
public:
  /// @brief get the eight corner points of an entity in a local coordinate system where the origin is at the center
  /// of the bounding box and the orientation is the entity's orientation.
  /// @param ref_entity   input entity which the corner points will be used as input
  /// @return vector of the entity's 8 corner points
  static std::vector<mantle_api::Vec3<units::length::meter_t>> GetBoundingBoxCornerPoints(
      const mantle_api::IEntity& ref_entity);

  /// @brief translate entity corner points to the global space coordinate system
  /// @param environment   environment interface
  /// @param position entity position
  /// @param orientation entity orientation
  /// @param local_corner_points entity corner points in the local coordinates
  /// @return entity corner points translated into the global coordinate system
  static std::vector<mantle_api::Vec3<units::length::meter_t>> GetBoundingBoxCornerPointsInGlobal(
      const std::shared_ptr<mantle_api::IEnvironment> environment,
      const mantle_api::Vec3<units::length::meter_t>& position,
      const mantle_api::Orientation3<units::angle::radian_t>& orientation,
      const std::vector<mantle_api::Vec3<units::length::meter_t>>& local_corner_points);

  /// @brief gets corner points of the entity in the given local coordinate system sorted by longitudinal distance
  /// @param environment       environment
  /// @param entity            entity
  /// @param local_origin      local coordinate system origin
  /// @param local_orientation local system orientation
  /// @return entity corner points positions  translated in the local coordinate system and sorted by the longitudinal
  /// distance from headmost to rearmost
  static std::vector<mantle_api::Vec3<units::length::meter_t>> GetCornerPositionsInLocalSortedByLongitudinalDistance(
      const std::shared_ptr<mantle_api::IEnvironment> environment,
      const mantle_api::IEntity& entity,
      const mantle_api::Vec3<units::length::meter_t>& local_origin,
      const mantle_api::Orientation3<units::angle::radian_t>& local_orientation);

  /// @brief calculate the longitudinal distance between closest bounding box points of the two entities
  /// in the master entity's coordinate system
  /// @param environment   environment interface
  /// @param master_entity it is the entity where its coordinate system will be used as local coordinate system
  /// @param reference_entity reference entity
  /// @return minimum longitudinal distance between two entities, calculated in the master's entity coordinate system
  static units::length::meter_t CalculateLongitudinalFreeSpaceDistance(
      const std::shared_ptr<mantle_api::IEnvironment> environment,
      const mantle_api::IEntity& master_entity,
      const mantle_api::IEntity& reference_entity);

  /// @brief calculate longitudinal distance between the origin of two entities in the master entity's local
  /// coordinate system
  /// @param environment   environment interface
  /// @param master_entity it is the entity where its coordinate system will be used as local coordinate system
  /// @param reference_entity reference entity
  /// @return longitudinal distance between origins of two entities, calculated in the master entity coordinate system
  static units::length::meter_t CalculateRelativeLongitudinalDistance(
      const std::shared_ptr<mantle_api::IEnvironment> environment,
      const mantle_api::IEntity& master_entity,
      const mantle_api::IEntity& reference_entity);

  /// @brief Get entity object from a given name
  /// @param environment environment interface
  /// @param entity_name name of the entity
  static mantle_api::IEntity& GetEntityByName(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                              const std::string& entity_name);
};

}  // namespace OPENSCENARIO