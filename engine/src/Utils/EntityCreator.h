/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

namespace OPENSCENARIO
{
class EntityCreator
{
public:
  explicit EntityCreator(std::shared_ptr<mantle_api::IEnvironment> environment);

  void CreateEntity(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object);

private:
  void CreateVehicle(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVehicle> vehicle, const std::string& name);
  void CreatePedestrian(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPedestrian> pedestrian,
                        const std::string& name);
  void CreateMiscObject(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IMiscObject> misc_object,
                        const std::string& name);
  void CreateCatalogReferenceEntity(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalog_reference,
                                    const std::string& name);

  void FillBoundingBoxProperties(mantle_api::EntityProperties& properties,
                                 std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IBoundingBox> bounding_box,
                                 const std::string& name);
  void FillEntityPropertiesForPedestrian(mantle_api::EntityProperties& properties,
                                         std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPedestrian> pedestrian);
  void FillGenericProperties(mantle_api::EntityProperties& entity_properties,
                             const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IProperty>>& properties);
  void FillAxleProperties(mantle_api::Axle& axle,
                          std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAxle> open_scenario_axle,
                          std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IBoundingBox> bounding_box,
                          const std::string& name);
  mantle_api::VehicleClass GetVehicleClass(NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory vehicle_category);
  mantle_api::EntityType GetEntityTypeFromPedestrianCategory(
      NET_ASAM_OPENSCENARIO::v1_1::PedestrianCategory pedestrian_category);

  void SetVerticalOffset(mantle_api::StaticObjectProperties& entity_properties, const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IProperty>>& properties);

  std::shared_ptr<mantle_api::IEnvironment> environment_;
};

}  // namespace OPENSCENARIO
