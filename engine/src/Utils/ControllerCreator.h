/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>
#include <MantleAPI/Execution/i_environment.h>

namespace OPENSCENARIO
{
class ControllerCreator
{
public:
  explicit ControllerCreator(std::shared_ptr<mantle_api::IEnvironment> environment);

  void CreateControllers(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object);

  void SetExternalControllerOverride();

private:
  bool IsControllableObject(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object);
  bool IsCatalogReferenceControllableObject(
      std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalog_reference);

  void SetupEntityWithInternalConfigs(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object);
  void SetupEntityWithExternalConfigs(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object);
  void AddEntityToController(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object, const mantle_api::UniqueId& id);
  std::unique_ptr<mantle_api::ExternalControllerConfig> CreateExternalControllerConfig(
      const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IController> controller);

  std::shared_ptr<mantle_api::IEnvironment> environment_;
  bool is_host_controlled_externally_{false};
};

}  // namespace OPENSCENARIO
