################################################################################
# Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

from model import UmlClass, Converter, is_leaf, parse_mappings

TEST_DATA = {
    "TestNode": {
        "properties": {
            "name": {
                "isList": False,
                "isXorElement": False,
                "type.name": "string",
                "type.isPrimitiveType": True,
                "type.isEnumeration": False
            },
            "rule": {
                "isList": False,
                "isXorElement": False,
                "isXorElement": False,
                "type.name": "Rule",
                "type.isPrimitiveType": False,
                "type.isEnumeration": True
            },
            "listItems": {
                "isList": True,
                "isXorElement": False,
                "type.name": "ListItem",
                "type.isPrimitiveType": False,
                "type.isEnumeration": False
            },
            "startTrigger": {
                "isList": False,
                "isXorElement": False,
                "type.name": "Trigger",
                "type.isPrimitiveType": False,
                "type.isEnumeration": False
            },
            "stopTrigger": {
                "isList": False,
                "isXorElement": False,
                "type.name": "Trigger",
                "type.isPrimitiveType": False,
                "type.isEnumeration": False
            }
        }
    }
}


def test_annotations__model_has_only_primitive_childs__is_leaf():
    TEST_DATA = {"TestNode": {"properties": {
        "name": {
            "isList": False,
            "isXorElement": False,
            "type.name": "string",
            "type.isPrimitiveType": True,
            "type.isEnumeration": False
        }
    }
    }}
    uml_class = UmlClass(TEST_DATA, 'TestNode')
    assert is_leaf(uml_class, {}) == True


def test_annotations__model_has_only_enums__is_leaf():
    TEST_DATA = {"TestNode": {"properties": {
        "rule": {
            "isList": False,
            "isXorElement": False,
            "type.name": "string",
            "type.isPrimitiveType": False,
            "type.isEnumeration": True
        }
    }
    }}
    uml_class = UmlClass(TEST_DATA, 'TestNode')
    assert is_leaf(uml_class, {}) == True


def test_annotations__model_has_only_mapped_types__is_leaf():
    TEST_DATA = {"TestNode": {"properties": {
        "mappedType": {
            "isList": False,
            "isXorElement": False,
            "type.name": "MappedType",
            "type.isPrimitiveType": False,
            "type.isEnumeration": False
        }
    }
    }}
    metainfo = {'MappedType':
                {
                    'return_type': 'dont_care',
                    'name': 'dont_care',
                    'include': '"dont_care"',
                    'dependencies': []
                }}

    uml_class = UmlClass(TEST_DATA, 'TestNode')
    assert is_leaf(uml_class, metainfo) == True


def test_annotations__model_has_unmapped_types__is_not_leaf():
    TEST_DATA = {"TestNode": {"properties": {
        "unmappedType": {
            "isList": False,
            "isXorElement": False,
            "type.name": "UnmappedType",
            "type.isPrimitiveType": False,
            "type.isEnumeration": False
        }
    }
    }}

    uml_class = UmlClass(TEST_DATA, 'TestNode')
    assert is_leaf(uml_class, {}) == False


def test_annotations__model_has_mapped_types_with_dependency__returns_converters_with_resolved_dependency():
    TEST_DATA = {"TestNode": {"properties": {
        "mappedType": {
            "isList": False,
            "isXorElement": False,
            "type.name": "MappedType",
            "type.isPrimitiveType": False,
            "type.isEnumeration": False
        }
    }
    }}

    dependencies = {'With': {
        'name': 'with',
        'type': 'dependency_type',
        'include': '"with.h"'
    }}
    converters = {'MappedType': {
        'return_type': 'std::string',
        'name': 'convert_method',
        'include': '"converter.h"',
        'dependencies': ['With']
    }}

    uml_class = UmlClass(TEST_DATA, 'TestNode')
    mapping = parse_mappings(uml_class, converters, dependencies)
    assert len(mapping) == 1
    assert mapping['MappedType'].return_type == 'std::string'
    assert mapping['MappedType'].name == 'convert_method'
    assert mapping['MappedType'].include == '"converter.h"'

    # note that dependencies are resolved as members! (right now => trailing '_')
    assert mapping['MappedType'].call('variable') == 'convert_method(with_, variable)'


def test_annotations__model_has_mapped_types_without_dependency__returns_converters_with_no_dependency():
    TEST_DATA = {"TestNode": {"properties": {
        "mappedType": {
            "isList": False,
            "isXorElement": False,
            "type.name": "MappedType",
            "type.isPrimitiveType": False,
            "type.isEnumeration": False
        }
    }
    }}

    converters = {'MappedType': {
        'return_type': 'bool',
        'name': 'convert_method',
        'include': '"converter.h"',
        'dependencies': []
    }}

    uml_class = UmlClass(TEST_DATA, 'TestNode')
    mapping = parse_mappings(uml_class, converters, {})
    assert len(mapping) == 1
    assert mapping['MappedType'].return_type == 'bool'
    assert mapping['MappedType'].name == 'convert_method'
    assert mapping['MappedType'].include == '"converter.h"'
    assert mapping['MappedType'].call('variable') == 'convert_method(variable)'
