/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StateMachine/IStateMachine.h"

namespace OPENSCENARIO
{

class StateMachine : public IStateMachine
{
  public:
    void Step() override;

    void SetActiveState(IState& new_active_state) override { active_state_ = &new_active_state; }

    const IState* GetActiveState() override { return active_state_; }

  private:
    IState* active_state_{nullptr};
};
}  // namespace OPENSCENARIO
