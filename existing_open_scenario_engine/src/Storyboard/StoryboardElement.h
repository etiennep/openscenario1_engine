/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StateMachine/StateMachine.h"
#include "StoryboardElementStates/CompleteState.h"
#include "StoryboardElementStates/RunningState.h"
#include "StoryboardElementStates/StandbyState.h"

namespace OPENSCENARIO
{

class StoryboardElement
{
  public:
    StoryboardElement();

    void Start();
    void End();
    void Stop();
    bool isStandby();
    bool isRunning();
    bool isComplete();

    virtual void Step() = 0;

  protected:
    StateMachine state_machine_;
    StandbyState standby_state_;
    RunningState running_state_;
    CompleteState complete_state_;

  private:
    // Can be overwritten in derived classes, if specific actions have to be performed in the transition
    virtual void ExecuteStartTransition(){};
    virtual void ExecuteEndTransition(){};
    virtual void ExecuteStopTransition(){};
};

}  // namespace OPENSCENARIO
