/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h"

#include <MantleAPI/Execution/i_environment.h>

namespace OPENSCENARIO
{

inline mantle_api::Shape ConvertToMantleApi(const NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape& from)
{
    if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::STEP)
    {
        return mantle_api::Shape::kStep;
    }
    if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::CUBIC)
    {
        return mantle_api::Shape::kCubic;
    }
    if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::LINEAR)
    {
        return mantle_api::Shape::kLinear;
    }
    if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsShape::SINUSOIDAL)
    {
        return mantle_api::Shape::kSinusoidal;
    }

    return mantle_api::Shape::kUndefined;
}

inline mantle_api::Dimension ConvertToMantleApi(const NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension& from)
{
    if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::TIME)
    {
        return mantle_api::Dimension::kTime;
    }
    if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::DISTANCE)
    {
        return mantle_api::Dimension::kDistance;
    }
    if (from == NET_ASAM_OPENSCENARIO::v1_1::DynamicsDimension::RATE)
    {
        return mantle_api::Dimension::kRate;
    }
    return mantle_api::Dimension::kUndefined;
}

inline mantle_api::TransitionDynamics ConvertToMantleApi(const NET_ASAM_OPENSCENARIO::v1_1::ITransitionDynamics& from)
{
    mantle_api::TransitionDynamics transition_dynamics{};
    transition_dynamics.value = from.GetValue();
    transition_dynamics.dimension = ConvertToMantleApi(from.GetDynamicsDimension());
    transition_dynamics.shape = ConvertToMantleApi(from.GetDynamicsShape());

    return transition_dynamics;
}

}  // namespace OPENSCENARIO
