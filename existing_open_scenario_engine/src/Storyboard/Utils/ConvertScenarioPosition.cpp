/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ConvertScenarioPosition.h"

#include <iostream>

namespace OPENSCENARIO
{

namespace detail
{
void FillOrientation(std::shared_ptr<mantle_api::IEnvironment> environment,
                     std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOrientation> orientation_ptr,
                     mantle_api::Pose& pose)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation{};
    if (orientation_ptr != nullptr)
    {
        orientation = mantle_api::Orientation3<units::angle::radian_t>{units::angle::radian_t(orientation_ptr->GetH()),
                                                                       units::angle::radian_t(orientation_ptr->GetP()),
                                                                       units::angle::radian_t(orientation_ptr->GetR())};

        if (orientation_ptr->GetType() == NET_ASAM_OPENSCENARIO::v1_1::ReferenceContext::RELATIVE)
        {

            auto lane_orientation = environment->GetQueryService().GetLaneOrientation(pose.position);
            pose.orientation = lane_orientation + orientation;
        }
        else
        {
            pose.orientation = orientation;
        }
    }
}
}  // namespace detail

mantle_api::Pose ConvertScenarioWorldPosition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IWorldPosition> scenario_world_position)
{
    mantle_api::Pose pose{};
    pose.position.x = units::length::meter_t{scenario_world_position->GetX()};
    pose.position.y = units::length::meter_t{scenario_world_position->GetY()};
    pose.position.z = units::length::meter_t{scenario_world_position->GetZ()};
    // There is only an absolute orientation for a world position according to the standard
    pose.orientation.yaw = units::angle::radian_t{scenario_world_position->GetH()};
    pose.orientation.pitch = units::angle::radian_t{scenario_world_position->GetP()};
    pose.orientation.roll = units::angle::radian_t{scenario_world_position->GetR()};
    return pose;
}

mantle_api::Pose ConvertScenarioRelativeLanePosition(
    std::shared_ptr<mantle_api::IEnvironment> environment,
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeLanePosition> scenario_relative_lane_position)
{
    mantle_api::Pose pose{};
    if (scenario_relative_lane_position->IsSetDs())
    {
        std::cout << "Warning: \"ds\" is not implemented for RelativeLanePosition." << std::endl;
        return pose;
    }

    if (!scenario_relative_lane_position->IsSetDsLane())
    {
        std::cout << "Warning: \"dsLane\" is not set, required for RelativeLanePosition." << std::endl;
        return pose;
    }

    const auto ref_entity_name = scenario_relative_lane_position->GetEntityRef()->GetNameRef();
    if (const auto ref_entity = environment->GetEntityRepository().Get(ref_entity_name))
    {
        const auto d_lane = scenario_relative_lane_position->GetDLane();
        const auto ds_lane = scenario_relative_lane_position->GetDsLane();
        const auto offset = scenario_relative_lane_position->GetOffset();

        const mantle_api::Pose pose_ref{environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
                                            ref_entity.value().get().GetPosition(),
                                            ref_entity.value().get().GetOrientation(),
                                            -ref_entity.value().get().GetProperties()->bounding_box.geometric_center),
                                        ref_entity.value().get().GetOrientation()};

        const auto calculated_pose = environment->GetQueryService().FindRelativeLanePoseAtDistanceFrom(
            pose_ref, d_lane, units::length::meter_t(ds_lane), units::length::meter_t(offset));
        if (calculated_pose.has_value())
        {
            pose.position = calculated_pose.value().position;
            detail::FillOrientation(environment, scenario_relative_lane_position->GetOrientation(), pose);
            return pose;
        }
        else
        {
            std::cout << "Warning: Conversion from RelativeLanePosition is not possible. Please check the scenario."
                      << std::endl;
        }
    }
    else
    {
        std::cout << "Warning: entity with name = \"" << ref_entity_name
                  << "\" for RelativeLanePosition does not exist. Please adjust the scenario." << std::endl;
    }
    return pose;
}

std::optional<mantle_api::Pose> ConvertScenarioPosition(std::shared_ptr<mantle_api::IEnvironment> environment,
                                                        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> pos)
{
    const auto* mantle_converter = environment->GetConverter();

    mantle_api::Pose pose{};

    if (auto scenario_world_position = pos->GetWorldPosition())
    {
        return ConvertScenarioWorldPosition(scenario_world_position);
    }
    else if (auto scenario_lane_position = pos->GetLanePosition())
    {
        mantle_api::OpenDriveLanePosition lane_position;
        lane_position.road = scenario_lane_position->GetRoadId();
        lane_position.lane = atoi(scenario_lane_position->GetLaneId().c_str());
        lane_position.s_offset = units::length::meter_t(scenario_lane_position->GetS());
        lane_position.t_offset = units::length::meter_t(scenario_lane_position->GetOffset());
        pose.position = mantle_converter->Convert(lane_position);

        detail::FillOrientation(environment, scenario_lane_position->GetOrientation(), pose);
        return pose;
    }
    else if (auto scenario_relative_lane_position = pos->GetRelativeLanePosition())
    {
        return ConvertScenarioRelativeLanePosition(environment, scenario_relative_lane_position);
    }
    else if (auto scenario_geo_position = pos->GetGeoPosition())
    {
        mantle_api::LatLonPosition geo_position{units::angle::radian_t(scenario_geo_position->GetLatitude()),
                                                units::angle::radian_t(scenario_geo_position->GetLongitude())};
        pose.position = mantle_converter->Convert(geo_position);

        detail::FillOrientation(environment, scenario_geo_position->GetOrientation(), pose);
        return pose;
    }
    else
    {
        std::cout << "Conversion from this type of OpenSCENARIO position to mantle API position not implemented yet"
                  << std::endl;

        return {};
    }
}

}  // namespace OPENSCENARIO
