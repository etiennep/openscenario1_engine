/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h"

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>

#include <memory>

namespace OPENSCENARIO
{

std::optional<mantle_api::Pose> ConvertScenarioPosition(std::shared_ptr<mantle_api::IEnvironment> environment,
                                                        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPosition> pos);

}  // namespace OPENSCENARIO
