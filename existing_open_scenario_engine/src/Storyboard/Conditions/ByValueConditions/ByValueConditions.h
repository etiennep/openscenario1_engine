/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "ParameterCondition.h"
#include "SimulationTimeCondition.h"
#include "StoryboardElementStateCondition.h"
#include "TimeOfDayCondition.h"
#include "TrafficSignalCondition.h"
#include "TrafficSignalControllerCondition.h"
#include "UserDefinedValueCondition.h"

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <variant>

namespace OPENSCENARIO
{

using ByValueCondition = std::variant<ParameterCondition,
                                      SimulationTimeCondition,
                                      StoryboardElementStateCondition,
                                      TimeOfDayCondition,
                                      TrafficSignalCondition,
                                      TrafficSignalControllerCondition,
                                      UserDefinedValueCondition>;

template <typename... Params>
ByValueCondition toCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IByValueCondition> condition,
                             Params&&... args)
{
    if (auto c = condition->GetParameterCondition(); c)
    {
        return to<ParameterCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetTimeOfDayCondition(); c)
    {
        return to<TimeOfDayCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetSimulationTimeCondition(); c)
    {
        return to<SimulationTimeCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetStoryboardElementStateCondition(); c)
    {
        return to<StoryboardElementStateCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetUserDefinedValueCondition(); c)
    {
        return to<UserDefinedValueCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetTrafficSignalCondition(); c)
    {
        return to<TrafficSignalCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetTrafficSignalControllerCondition(); c)
    {
        return to<TrafficSignalControllerCondition>(c, std::forward<Params>(args)...);
    }
    throw std::runtime_error("Cannot specialize IByValueCondition");
}

}  // namespace OPENSCENARIO
