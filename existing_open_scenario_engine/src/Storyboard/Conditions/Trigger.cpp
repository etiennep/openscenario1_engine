/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Trigger.h"

namespace OPENSCENARIO
{

OPENSCENARIO::Trigger::Trigger(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrigger> trigger_data,
                               std::shared_ptr<mantle_api::IEnvironment> environment)
    : data_(trigger_data)
{
    for (auto condition_group : trigger_data->GetConditionGroups())
    {
        condition_groups.emplace_back(ConditionGroup(condition_group, environment));
    }
}

bool Trigger::IsSatisfied()
{
    // See OpenSCENARIO 1.1 User Guide:
    // OpenSCENARIO 1.0 allows instances of the Trigger class to be empty, for example,
    // no condition groups defined. Empty triggers are not meaningful and are kept for the sake of downward
    // compatibility. This is expected to be addressed in the next major version where a trigger shall be defined with
    // at least one condition group. An empty trigger is always evaluated to false.
    return std::any_of(condition_groups.begin(), condition_groups.end(), [](auto& c) { return c.IsSatisfied(); });
}

}  // namespace OPENSCENARIO
