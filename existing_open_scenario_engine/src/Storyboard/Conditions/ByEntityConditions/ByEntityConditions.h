/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "AccelerationCondition.h"
#include "CollisionCondition.h"
#include "DistanceCondition.h"
#include "EndOfRoadCondition.h"
#include "OffroadCondition.h"
#include "ReachPositionCondition.h"
#include "RelativeDistanceCondition.h"
#include "RelativeSpeedCondition.h"
#include "SpeedCondition.h"
#include "StandStillCondition.h"
#include "TimeHeadwayCondition.h"
#include "TimeToCollisionCondition.h"
#include "TraveledDistanceCondition.h"

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <variant>

namespace OPENSCENARIO
{

using ByEntityCondition = std::variant<AccelerationCondition,
                                       CollisionCondition,
                                       DistanceCondition,
                                       EndOfRoadCondition,
                                       OffroadCondition,
                                       ReachPositionCondition,
                                       RelativeDistanceCondition,
                                       RelativeSpeedCondition,
                                       SpeedCondition,
                                       StandStillCondition,
                                       TimeHeadwayCondition,
                                       TimeToCollisionCondition,
                                       TraveledDistanceCondition>;

template <typename TargetType, typename Payload, typename... Params>
auto to(Payload payload, Params&&... args)
{
    return TargetType(payload, std::forward<Params>(args)...);
}

template <typename... Params>
ByEntityCondition toCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityCondition> condition,
                              Params&&... args)
{
    if (auto c = condition->GetEndOfRoadCondition(); c)
    {
        return to<EndOfRoadCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetCollisionCondition(); c)
    {
        return to<CollisionCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetOffroadCondition(); c)
    {
        return to<OffroadCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetTimeHeadwayCondition(); c)
    {
        return to<TimeHeadwayCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetTimeToCollisionCondition(); c)
    {
        return to<TimeToCollisionCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetAccelerationCondition(); c)
    {
        return to<AccelerationCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetStandStillCondition(); c)
    {
        return to<StandStillCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetSpeedCondition(); c)
    {
        return to<SpeedCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetRelativeSpeedCondition(); c)
    {
        return to<RelativeSpeedCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetTraveledDistanceCondition(); c)
    {
        return to<TraveledDistanceCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetReachPositionCondition(); c)
    {
        return to<ReachPositionCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetDistanceCondition(); c)
    {
        return to<DistanceCondition>(c, std::forward<Params>(args)...);
    }
    if (auto c = condition->GetRelativeDistanceCondition(); c)
    {
        return to<RelativeDistanceCondition>(c, std::forward<Params>(args)...);
    }

    throw std::runtime_error("Cannot specialize IByEntityCondition");
}

}  // namespace OPENSCENARIO
