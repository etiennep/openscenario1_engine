/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG),
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ConditionGroup.h"

#include <algorithm>

namespace OPENSCENARIO
{

namespace
{
template <typename TargetType, typename... Params>
auto to_unique(Params&&... args)
{
    return [&](auto payload) { return std::make_unique<TargetType>(payload, std::forward<Params>(args)...); };
}
}  // namespace

ConditionGroup::ConditionGroup(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IConditionGroup> condition_group_data,
                               std::shared_ptr<mantle_api::IEnvironment> environment)
{
    const auto& conditions = condition_group_data->GetConditions();
    conditions_.reserve(conditions.size());
    std::transform(
        conditions.begin(), conditions.end(), std::back_inserter(conditions_), to_unique<Condition>(environment));
}

bool ConditionGroup::IsSatisfied()
{
    return std::all_of(conditions_.begin(), conditions_.end(), [](auto& c) { return c->IsSatisfied(); });
}

}  // namespace OPENSCENARIO
