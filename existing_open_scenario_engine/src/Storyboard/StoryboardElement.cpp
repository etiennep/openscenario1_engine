/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StoryboardElement.h"

namespace OPENSCENARIO
{
StoryboardElement::StoryboardElement()
{
    state_machine_.SetActiveState(standby_state_);
}

void StoryboardElement::Start()
{
    ExecuteStartTransition();
    state_machine_.SetActiveState(running_state_);
}

void StoryboardElement::End()
{
    ExecuteEndTransition();
    state_machine_.SetActiveState(complete_state_);
}

void StoryboardElement::Stop()
{
    ExecuteStopTransition();
    state_machine_.SetActiveState(complete_state_);
}

bool StoryboardElement::isStandby()
{
    return state_machine_.GetActiveState() == &standby_state_;
}

bool StoryboardElement::isRunning()
{
    return state_machine_.GetActiveState() == &running_state_;
}

bool StoryboardElement::isComplete()
{
    return state_machine_.GetActiveState() == &complete_state_;
}

}  // namespace OPENSCENARIO
