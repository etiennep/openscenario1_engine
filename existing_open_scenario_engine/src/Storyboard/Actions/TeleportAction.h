/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "PrivateAction.h"

#include <MantleAPI/Common/pose.h>

#include <vector>

namespace OPENSCENARIO
{

class TeleportAction : public PrivateAction
{
  public:
    TeleportAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITeleportAction> teleport_action_data,
                   std::shared_ptr<mantle_api::IEnvironment> environment,
                   const std::vector<std::string>& actors);

    void ExecuteStartTransition() override;

    void Step() override;

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITeleportAction> GetTeleportActionData()
    {
        return teleport_action_data_;
    }

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITeleportAction> teleport_action_data_;
};

}  // namespace OPENSCENARIO
