/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/Actions/Action.h"

#include <vector>

namespace OPENSCENARIO
{

class UserDefinedAction : public Action
{
  public:
    UserDefinedAction(std::shared_ptr<mantle_api::IEnvironment> environment, const std::vector<std::string>& actors);

    std::vector<std::string> GetActors() { return actors_; }

  protected:
    std::vector<std::string> actors_;
};

}  // namespace OPENSCENARIO
