/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StateMachine/IState.h"

namespace OPENSCENARIO
{

class CompleteState : public IState
{
  public:
    void Step() override {}
    void SetContext(IStateMachine& context) override { std::ignore = context; }
    void SetTransitionToState(IState& next_state) override { std::ignore = next_state; }
};

}  // namespace OPENSCENARIO
