/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StateMachine/IState.h"
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

namespace OPENSCENARIO
{
class StandbyState : public IState
{
  public:
    void Step() override;

    void SetContext(IStateMachine& context) override;

    void SetTransitionToState(IState& next_state) override;

  protected:
    void TransitionToNextState();

    IStateMachine* context_{nullptr};
    IState* next_state_{nullptr};
};

}  // namespace OPENSCENARIO
