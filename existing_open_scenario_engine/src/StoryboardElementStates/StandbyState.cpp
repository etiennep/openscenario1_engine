/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StandbyState.h"

namespace OPENSCENARIO
{

void StandbyState::Step()
{
    TransitionToNextState();
}

void StandbyState::SetContext(IStateMachine& context)
{
    context_ = &context;
}

void StandbyState::SetTransitionToState(IState& next_state)
{
    next_state_ = &next_state;
}

void StandbyState::TransitionToNextState()
{
    if (context_ == nullptr)
    {
        throw std::runtime_error(
            "InitState was stepped without a valid context. Transition to next state not possible");
    }

    if (next_state_ == nullptr)
    {
        throw std::runtime_error(
            "InitState was stepped without a valid next state. Call SetTransitionToState before Step");
    }

    context_->SetActiveState(*next_state_);
}

}  // namespace OPENSCENARIO
