/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "builders/storyboard_builder.h"

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

namespace OPENSCENARIO::TESTING
{
FakeTriggerBuilder& FakeTriggerBuilder::WithConditionGroup(FakeConditionGroup condition_group)
{
    trigger_.AddConditionGroup(std::move(condition_group));
    return *this;
}

std::shared_ptr<FakeTrigger> FakeTriggerBuilder::Build()
{
    return std::make_shared<FakeTrigger>(trigger_);
}

std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IConditionGroup>> FakeTrigger::GetConditionGroups() const
{
    return condition_groups_;
}

void FakeTrigger::AddConditionGroup(FakeConditionGroup condition_group)
{
    condition_groups_.push_back(std::make_shared<FakeConditionGroup>(std::move(condition_group)));
}

void FakeEvent::AddAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActionWriter> action)
{
    fake_actions.push_back(action);
    SetActions(fake_actions);
}

FakeEventBuilder::FakeEventBuilder() : event_(std::make_shared<FakeEvent>()) {}

FakeEventBuilder& FakeEventBuilder::WithAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActionWriter> action)
{
    event_->AddAction(action);
    return *this;
}

FakeEventBuilder& FakeEventBuilder::WithStartTrigger(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggerWriter> trigger)
{
    event_->SetStartTrigger(trigger);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEventWriter> FakeEventBuilder::Build()
{
    return event_;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IInit> FakeStoryboard::GetInit() const
{
    return IStoryboard::GetInit();
}

std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IStory>> FakeStoryboard::GetStories() const
{
    return IStoryboard::GetStories();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrigger> FakeStoryboard::GetStopTrigger() const
{
    return stop_trigger_;
}

void FakeStoryboard::SetStopTrigger(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggerWriter> stop_trigger)
{
    stop_trigger_ = std::move(stop_trigger);
}

FakeStoryboardBuilder& FakeStoryboardBuilder::WithStopTrigger(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggerWriter> stop_trigger)
{
    storyboard_.SetStopTrigger(std::move(stop_trigger));
    return *this;
}

FakeStoryboard FakeStoryboardBuilder::Build()
{
    return storyboard_;
}

NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesImpl FakeTriggerEntitiesBuilder::Build()
{
    return trigger_entities_;
}

FakeTriggerEntitiesBuilder& FakeTriggerEntitiesBuilder::WithTriggeringEntitiesRule(
    const NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum rule)
{
    trigger_entities_.SetTriggeringEntitiesRule(NET_ASAM_OPENSCENARIO::v1_1::TriggeringEntitiesRule(rule));
    return *this;
}

FakeTriggerEntitiesBuilder& FakeTriggerEntitiesBuilder::WithEntityRefs(std::vector<std::string> trigger_entities_names)
{

    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEntityRefWriter>> fake_entity_refs;
    for (const auto& entity_name : trigger_entities_names)
    {
        auto fake_entity_ref = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::EntityRefImpl>();
        auto fake_named_ref =
            std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>(
                entity_name);
        fake_entity_ref->SetEntityRef(fake_named_ref);
        fake_entity_refs.push_back(fake_entity_ref);
    }
    trigger_entities_.SetEntityRefs(fake_entity_refs);
    return *this;
}

}  // namespace OPENSCENARIO::TESTING
