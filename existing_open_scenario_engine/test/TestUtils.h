/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "ControllerCreator.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/src/common/SimpleMessageLogger.h>
#include <openScenarioLib/src/loader/FileResourceLocator.h>
#include <openScenarioLib/src/v1_1/loader/XmlScenarioImportLoaderFactoryV1_1.h>

#include <filesystem>
#include <string>

namespace OPENSCENARIO
{
using namespace units::literals;

inline std::string GetScenariosPath(const std::string& test_file_path)
{
    std::filesystem::path file_path{test_file_path};
    return {file_path.remove_filename().string() + "data/Scenarios/"};
}

class OpenScenarioEngineLibraryTestBase : public ::testing::Test
{
  protected:
    void SetUp() override
    {
        env_ = std::make_shared<mantle_api::MockEnvironment>();
        ON_CALL(env_->GetControllerRepository(), Create(testing::_)).WillByDefault(testing::ReturnRef(controller_));
    }

    std::shared_ptr<mantle_api::MockEnvironment> env_;
    mantle_api::MockController controller_;
    std::string default_xosc_scenario_{
        "AutomatedLaneKeepingSystemScenarios/ALKS_Scenario_4.1_1_FreeDriving_TEMPLATE.xosc"};
};

class EngineSubModuleTestBase : public OpenScenarioEngineLibraryTestBase
{
  protected:
    virtual void LoadScenario(const std::string& scenario_file_path)
    {
        auto message_logger =
            std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
        auto catalog_message_logger =
            std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
        auto loader_factory =
            NET_ASAM_OPENSCENARIO::v1_1::XmlScenarioImportLoaderFactory(catalog_message_logger, scenario_file_path);
        auto loader = loader_factory.CreateLoader(std::make_shared<NET_ASAM_OPENSCENARIO::FileResourceLocator>());

        scenario_ptr_ = std::static_pointer_cast<NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario>(
            loader->Load(message_logger)->GetAdapter(typeid(NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario).name()));
    }

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario> scenario_ptr_;
};

class OpenScenarioEngineTestBase : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override
    {
        OpenScenarioEngineLibraryTestBase::SetUp();

        mantle_api::VehicleProperties ego_properties{};
        ego_properties.type = mantle_api::EntityType::kVehicle;
        ego_properties.classification = mantle_api::VehicleClass::kMedium_car;
        ego_properties.model = "medium_car";
        ego_properties.bounding_box.dimension.length = 5.0_m;
        ego_properties.bounding_box.dimension.width = 2.0_m;
        ego_properties.bounding_box.dimension.height = 1.8_m;
        ego_properties.bounding_box.geometric_center.x = 1.4_m;
        ego_properties.bounding_box.geometric_center.y = 0.0_m;
        ego_properties.bounding_box.geometric_center.z = 0.9_m;
        ego_properties.performance.max_acceleration = 10_mps_sq;
        ego_properties.performance.max_deceleration = 10_mps_sq;
        ego_properties.performance.max_speed = 70_mps;
        ego_properties.front_axle.bb_center_to_axle_center = {1.58_m, 0.0_m, -0.5_m};
        ego_properties.front_axle.max_steering = 0.5_rad;
        ego_properties.front_axle.track_width = 1.68_m;
        ego_properties.front_axle.wheel_diameter = 0.8_m;
        ego_properties.rear_axle.bb_center_to_axle_center = {-1.4_m, 0.0_m, -0.5_m};
        ego_properties.rear_axle.max_steering = 0_rad;
        ego_properties.rear_axle.track_width = 1.68_m;
        ego_properties.rear_axle.wheel_diameter = 0.8_m;
        ego_properties.is_host = true;
        // Necessary because Create() is always called in engine init and will otherwise not return a MockVehicle ref
        // which results in an exception
        ON_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(testing::_, ego_properties))
            .WillByDefault(testing::ReturnRef(mock_vehicle_));
    }

    mantle_api::MockVehicle mock_vehicle_{};
};

}  // namespace OPENSCENARIO
