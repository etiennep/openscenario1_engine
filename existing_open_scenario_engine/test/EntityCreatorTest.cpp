/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EntityCreator.h"
#include "TestUtils.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace units::literals;

class EntityCreatorTest : public EngineSubModuleTestBase
{
};

MATCHER_P(StaticObjectPropertiesEqualTo, expected_static_object_property, "")
{
    return arg.type == expected_static_object_property.type && arg.model == expected_static_object_property.model &&
           arg.vertical_offset == expected_static_object_property.vertical_offset &&
           arg.bounding_box.dimension.length == expected_static_object_property.bounding_box.dimension.length &&
           arg.bounding_box.dimension.width == expected_static_object_property.bounding_box.dimension.width &&
           arg.bounding_box.dimension.height == expected_static_object_property.bounding_box.dimension.height &&
           arg.bounding_box.geometric_center.x == expected_static_object_property.bounding_box.geometric_center.x &&
           arg.bounding_box.geometric_center.y == expected_static_object_property.bounding_box.geometric_center.y &&
           arg.bounding_box.geometric_center.z == expected_static_object_property.bounding_box.geometric_center.z;
}

TEST_F(EntityCreatorTest, GivenScenarioWithEntities_WhenCreateEntityIsCalled_ThenCreateIsCalledInEnvironment)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "test_entities_valid.xosc"};
    LoadScenario(xosc_file_path);
    OPENSCENARIO::EntityCreator entity_creator(env_);

    mantle_api::MockVehicle mock_vehicle{};
    mantle_api::MockPedestrian mock_pedestrian{};
    mantle_api::MockStaticObject mock_static_object{};

    mantle_api::VehicleProperties vehicle_properties{};
    vehicle_properties.type = mantle_api::EntityType::kVehicle;
    vehicle_properties.classification = mantle_api::VehicleClass::kMotorbike;
    vehicle_properties.model = "motorbike";
    vehicle_properties.bounding_box.dimension.length = 5.0_m;
    vehicle_properties.bounding_box.dimension.width = 2.0_m;
    vehicle_properties.bounding_box.dimension.height = 1.8_m;
    vehicle_properties.bounding_box.geometric_center.x = 1.4_m;
    vehicle_properties.bounding_box.geometric_center.y = 0.0_m;
    vehicle_properties.bounding_box.geometric_center.z = 0.9_m;
    vehicle_properties.performance.max_acceleration = 10_mps_sq;
    vehicle_properties.performance.max_deceleration = 10_mps_sq;
    vehicle_properties.performance.max_speed = 70_mps;
    vehicle_properties.front_axle.bb_center_to_axle_center = {1.58_m, 0.0_m, -0.5_m};
    vehicle_properties.front_axle.max_steering = 0.5_rad;
    vehicle_properties.front_axle.track_width = 1.68_m;
    vehicle_properties.front_axle.wheel_diameter = 0.8_m;
    vehicle_properties.rear_axle.bb_center_to_axle_center = {-1.40_m, 0.0_m, -0.5_m};
    vehicle_properties.rear_axle.max_steering = 0_rad;
    vehicle_properties.rear_axle.track_width = 1.68_m;
    vehicle_properties.rear_axle.wheel_diameter = 0.8_m;

    mantle_api::VehicleProperties ego_properties{vehicle_properties};
    ego_properties.classification = mantle_api::VehicleClass::kMedium_car;
    ego_properties.model = "medium_car";
    ego_properties.is_host = true;
    ego_properties.properties.emplace("custom_property", "5");

    mantle_api::VehicleProperties wheelchair_properties{};
    wheelchair_properties.type = mantle_api::EntityType::kVehicle;
    wheelchair_properties.classification = mantle_api::VehicleClass::kWheelchair;
    wheelchair_properties.bounding_box.dimension.length = 0.7_m;
    wheelchair_properties.bounding_box.dimension.width = 0.7_m;
    wheelchair_properties.bounding_box.dimension.height = 1.3_m;
    wheelchair_properties.bounding_box.geometric_center.x = 0.35_m;
    wheelchair_properties.bounding_box.geometric_center.y = 0.0_m;
    wheelchair_properties.bounding_box.geometric_center.z = 0.65_m;

    mantle_api::PedestrianProperties pedestrian_properties{};
    pedestrian_properties.type = mantle_api::EntityType::kPedestrian;
    pedestrian_properties.model = "male";
    pedestrian_properties.bounding_box.dimension.length = 0.3_m;
    pedestrian_properties.bounding_box.dimension.width = 0.5_m;
    pedestrian_properties.bounding_box.dimension.height = 1.8_m;
    pedestrian_properties.bounding_box.geometric_center.x = 0.15_m;
    pedestrian_properties.bounding_box.geometric_center.y = 0.0_m;
    pedestrian_properties.bounding_box.geometric_center.z = 0.9_m;

    mantle_api::PedestrianProperties animal_properties{};
    animal_properties.type = mantle_api::EntityType::kAnimal;
    animal_properties.model = "Deer.gltf";
    animal_properties.bounding_box.dimension.length = 1.5_m;
    animal_properties.bounding_box.dimension.width = 0.5_m;
    animal_properties.bounding_box.dimension.height = 1.1_m;
    animal_properties.bounding_box.geometric_center.x = 0.75_m;
    animal_properties.bounding_box.geometric_center.y = 0.0_m;
    animal_properties.bounding_box.geometric_center.z = 0.55_m;

    mantle_api::StaticObjectProperties static_object_properties{};
    static_object_properties.type = mantle_api::EntityType::kOther;
    static_object_properties.model = "pylon";
    static_object_properties.vertical_offset = 0.5_m;
    static_object_properties.bounding_box.dimension.length = 1.0_m;
    static_object_properties.bounding_box.dimension.width = 1.0_m;
    static_object_properties.bounding_box.dimension.height = 1.0_m;
    static_object_properties.bounding_box.geometric_center.x = 0.5_m;
    static_object_properties.bounding_box.geometric_center.y = 0.0_m;
    static_object_properties.bounding_box.geometric_center.z = 0.5_m;

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("ScenarioObject6"), static_object_properties))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_static_object));

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("ScenarioObject5"), wheelchair_properties))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_vehicle));

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("ScenarioObject4"), animal_properties))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_pedestrian));

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("ScenarioObject3"), vehicle_properties))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_vehicle));

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("ScenarioObject2"),
                       testing::Matcher<const mantle_api::StaticObjectProperties&>(
                           StaticObjectPropertiesEqualTo(static_object_properties))))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_static_object));

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("ScenarioObject1"), pedestrian_properties))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_pedestrian));

    EXPECT_CALL(dynamic_cast<mantle_api::MockEntityRepository&>(env_->GetEntityRepository()),
                Create(std::string("Ego"), ego_properties))
        .Times(1)
        .WillRepeatedly(testing::ReturnRef(mock_vehicle));

    for (auto&& scenario_object :
         scenario_ptr_->GetOpenScenarioCategory()->GetScenarioDefinition()->GetEntities()->GetScenarioObjects())
    {
        entity_creator.CreateEntity(scenario_object);
    }
}

}  // namespace OPENSCENARIO
