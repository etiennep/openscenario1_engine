/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControllerCreator.h"
#include "TestUtils.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{

class ControllerCreatorTest : public EngineSubModuleTestBase
{
};

MATCHER(IsInternalConfig, "Determine if the object is of type mantle_api::InternalControllerConfig")
{
    if (arg == nullptr)
    {
        *result_listener << "is nullptr, and cannot be a mantle_api::InternalControllerConfig";
        return false;
    }
    auto config = dynamic_cast<mantle_api::InternalControllerConfig *>(arg.get());
    if (config == nullptr)
    {
        *result_listener << "is NOT a mantle_api::InternalControllerConfig";
        return false;
    }
    return true;
}

MATCHER(IsExternalConfig, "Determine if the object is of type mantle_api::ExternalControllerConfig")
{
    if (arg == nullptr)
    {
        *result_listener << "is nullptr, and cannot be a mantle_api::ExternalControllerConfig";
        return false;
    }
    auto config = dynamic_cast<mantle_api::ExternalControllerConfig *>(arg.get());
    if (config == nullptr)
    {
        *result_listener << "is NOT a mantle_api::ExternalControllerConfig";
        return false;
    }
    return true;
}

TEST_F(ControllerCreatorTest,
       GivenFiveControllableEntities_WhenCreateControllerIsCalled_ThenFiveControllersAreAddedInEnvironment)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "test_entities_valid.xosc"};
    LoadScenario(xosc_file_path);
    OPENSCENARIO::ControllerCreator controller_creator(env_);
    mantle_api::MockController controller;

    EXPECT_CALL(env_->GetControllerRepository(), Create(IsExternalConfig())).Times(1).WillRepeatedly(testing::ReturnRef(controller));
    EXPECT_CALL(env_->GetControllerRepository(), Create(IsInternalConfig())).Times(4).WillRepeatedly(testing::ReturnRef(controller));
    EXPECT_CALL(*env_, AddEntityToController(testing::_, testing::_)).Times(5);

    for (auto&& scenario_object :
         scenario_ptr_->GetOpenScenarioCategory()->GetScenarioDefinition()->GetEntities()->GetScenarioObjects())
    {
        controller_creator.CreateControllers(scenario_object);
    }
}

TEST_F(ControllerCreatorTest,
       GivenExternalControllerOverrideSet_WhenCreateControllerIsCalled_ThenHostGetsExternalConfig)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + "test_entities_valid.xosc"};
    LoadScenario(xosc_file_path);
    OPENSCENARIO::ControllerCreator controller_creator(env_);
    mantle_api::MockController controller;

    controller_creator.SetExternalControllerOverride();

    EXPECT_CALL(env_->GetControllerRepository(), Create(IsExternalConfig())).Times(2).WillRepeatedly(testing::ReturnRef(controller));
    EXPECT_CALL(env_->GetControllerRepository(), Create(IsInternalConfig())).Times(3).WillRepeatedly(testing::ReturnRef(controller));

    for (auto&& scenario_object :
         scenario_ptr_->GetOpenScenarioCategory()->GetScenarioDefinition()->GetEntities()->GetScenarioObjects())
    {
        controller_creator.CreateControllers(scenario_object);
    }
}

}  // namespace OPENSCENARIO
