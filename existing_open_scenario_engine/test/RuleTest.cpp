/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Conditions/Common/Rule.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{

TEST(RuleTest, GivenUnkownRule_WhenInstantiate_ThenThrow)
{
    EXPECT_ANY_THROW(Rule<double>(NET_ASAM_OPENSCENARIO::v1_1::Rule::UNKNOWN, 1));
}

TEST(RuleTest, GivenRuleWithGreaterThanOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_THAN, 1};

    EXPECT_FALSE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithGreaterThanOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_THAN, 1};

    EXPECT_FALSE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithGreaterThanOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_THAN, 1};

    EXPECT_TRUE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithGreaterOrEqualThanOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_OR_EQUAL, 1};

    EXPECT_FALSE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithGreaterOrEqualThanOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_OR_EQUAL, 1};

    EXPECT_TRUE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithGreaterOrEqualThanOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::GREATER_OR_EQUAL, 1};

    EXPECT_TRUE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithLessThanOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_THAN, 1};

    EXPECT_TRUE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithLessThanOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_THAN, 1};

    EXPECT_FALSE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithLessThanOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_THAN, 1};

    EXPECT_FALSE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithLessOrEqualOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_OR_EQUAL, 1};

    EXPECT_TRUE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithLessOrEqualOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_OR_EQUAL, 1};

    EXPECT_TRUE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithLessOrEqualOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::LESS_OR_EQUAL, 1};

    EXPECT_FALSE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithEqualToOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::EQUAL_TO, 1};

    EXPECT_FALSE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithEqualToOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::EQUAL_TO, 1};

    EXPECT_TRUE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithEqualToOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::EQUAL_TO, 1};

    EXPECT_FALSE(rule.IsSatisfied(2));
}

TEST(RuleTest, GivenRuleWithNotEqualToOne_WhenEvaluateWithZero_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::NOT_EQUAL_TO, 1};

    EXPECT_TRUE(rule.IsSatisfied(0));
}

TEST(RuleTest, GivenRuleWithNotEqualToOne_WhenEvaluateWithOne_ThenIsSatisfiedReturnsFalse)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::NOT_EQUAL_TO, 1};

    EXPECT_FALSE(rule.IsSatisfied(1));
}

TEST(RuleTest, GivenRuleWithNotEqualToOne_WhenEvaluateWithTwo_ThenIsSatisfiedReturnsTrue)
{
    OPENSCENARIO::Rule<double> rule{NET_ASAM_OPENSCENARIO::v1_1::Rule::NOT_EQUAL_TO, 1};

    EXPECT_TRUE(rule.IsSatisfied(2));
}

}  // namespace OPENSCENARIO
