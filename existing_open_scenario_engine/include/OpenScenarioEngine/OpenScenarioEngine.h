/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "ControllerCreator.h"
#include "EntityCreator.h"
#include "Storyboard/Storyboard.h"

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_scenario_engine.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>
#include <openScenarioLib/src/common/SimpleMessageLogger.h>

#include <memory>
#include <string>

namespace OPENSCENARIO
{
class OpenScenarioEngine : public mantle_api::IScenarioEngine
{
  public:
    using OpenScenarioPtr = std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario>;
    using ScenarioDefinitionPtr = std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioDefinition>;
    using SimpleMessageLoggerPtr = std::shared_ptr<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>;

    OpenScenarioEngine(const std::string& scenario_file_path, std::shared_ptr<mantle_api::IEnvironment> environment);

    void ActivateExternalHostControl() override;
    mantle_api::ScenarioInfo GetScenarioInfo() const override;
    void Init() override;
    bool IsFinished() const override;
    void Step() override;
    int ValidateScenario() override;

    OpenScenarioPtr GetScenario() { return scenario_data_ptr_; }

  protected:
    std::string scenario_file_path_;
    OpenScenarioPtr scenario_data_ptr_{nullptr};
    std::shared_ptr<NET_ASAM_OPENSCENARIO::SimpleMessageLogger> catalog_message_logger_;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::SimpleMessageLogger> message_logger_;
    void LoadScenarioData();

  private:
    /// If the path cannot be resolved by the scenario loader, override this function to resolve the path before loading
    /// in ParseScenarioFile().
    ///
    /// @param Scenario file in any given form
    /// @return Resolved path to the scenario file
    virtual std::string ResolveScenarioPath(const std::string& scenario_path) const { return scenario_path; }
    /// If the map path cannot be resolved by the environment, override this function to resolve the path before calling
    /// the environment's CreateMap().
    ///
    /// @param Scenario file in any given form
    /// @return Resolved path to the scenario file
    virtual std::string ResolveMapPath(const std::string& map_path) const { return map_path; }
    /// If it is used a different standard output system, override this function with the proper output
    ///
    /// @param message logger where the messages are extracted from
    /// @return void
    virtual void LogParsingMessages(SimpleMessageLoggerPtr message_logger);

    void ParseScenarioFile();
    void LoadRoadNetwork();
    void LoadUsedArea(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IUsedArea> used_area,
                      mantle_api::MapDetails& map_details);
    void CreateEntities();
    void CreateControllers();
    void CreateAndInitStoryboard();
    /// The default environment refers to excellent visibility, no precipitation, illumination as other,
    /// humidity at 60 percent, temperature at 15 degrees celsius and atmospheric pressure at sea level.
    /// The time of the day is considered as midday.
    ///
    /// @param void
    /// @return void
    void SetDefaultEnvironment();
    void SetScenarioDefinitionPtr(OpenScenarioPtr open_scenario_ptr);

    std::shared_ptr<mantle_api::IEnvironment> environment_;
    EntityCreator entity_creator_;
    std::shared_ptr<ControllerCreator> controller_creator_;
    const mantle_api::ICoordConverter* coordinate_converter_{nullptr};
    std::unique_ptr<Storyboard> storyboard_{nullptr};
    ScenarioDefinitionPtr scenario_definition_ptr_{nullptr};
};
}  // namespace OPENSCENARIO
